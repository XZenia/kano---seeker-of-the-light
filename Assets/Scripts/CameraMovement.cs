﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.125f;
    public float offset;

    void LateUpdate()
    {
        Vector3 position = transform.position;

        position.x = target.position.x;
        position.x += offset;

        position.y = target.position.y;


        transform.position = position;
    }
}
