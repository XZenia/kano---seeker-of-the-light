﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollowBehavior : StateMachineBehaviour
{
    private Transform playerPosition;
    public int movementSpeed;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetBool("IsFollowing"))
        {
            animator.transform.position = Vector2.MoveTowards(animator.transform.position, playerPosition.position, movementSpeed * Time.deltaTime);
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }
}
