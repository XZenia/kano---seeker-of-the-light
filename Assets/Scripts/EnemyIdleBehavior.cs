﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdleBehavior : StateMachineBehaviour
{
    private Transform playerPosition;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float distanceBetweenPlayerAndEnemy = Vector2.Distance(playerPosition.position, animator.transform.position);
        if (distanceBetweenPlayerAndEnemy < 10)
        {
            animator.SetBool("IsFollowing", true);
        }
        else
        {
            animator.SetBool("IsFollowing", false);
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
